echo "testing no args:"
python regr.py 2&>1 >/dev/null
if [ $? == '0' ];
then
  echo "argparsing fails"
else
  echo "test passes"
fi

echo "testing wrong path:"
python regr.py --csv-file ./test1.csv >/dev/null
if [ $? == '0' ];
then
  echo "path check fails"
else
  echo "test passes"
fi

echo "testing wrong file format:"
python regr.py --csv-file ./test.sh >/dev/null
if [ $? == '0' ];
then
  echo "csv filter check fails"
else
  echo "test passes"
fi

echo "testing wrong fbid field:"
python regr.py --csv-file ./test_wrong_fbid.csv >/dev/null
if [ $? == '0' ];
then
  echo "fbid filter check fails"
else
  echo "test passes"
fi

echo "testing valid csv that passes:"
python regr.py --csv-file ./test.csv >/dev/null
if [ $? != '0' ];
then
  echo "check fails"
else
  echo "test passes"
fi
