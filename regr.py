import csv
from datetime import datetime
import argparse
import os
import re

def file_reader(file_name):
    with open(file_name, 'r') as user_base:
        field_names = ('fbid', 'token', 'username')
        reader = csv.DictReader(user_base, fieldnames=field_names, delimiter=',')
        fbid_filter = re.compile(r'\d{13,16}')
        for user_row in reader:
            if len(user_row.items()) is not 3:
                raise Exception("csv string mistake, please check csv file")
            if not fbid_filter.match(user_row['fbid']):
                raise Exception("fbid field contains junk items")
            date_estimate = polinomial_interpolation(int(user_row['fbid'], 10))
            formated_date = datetime.fromtimestamp(date_estimate).strftime('%Y-%m-%d')
            print(','.join([user_row['username'], user_row[
                  'fbid'], str(date_estimate), formated_date]))


def polinomial_interpolation(fb_id):
    tmp_id = int(fb_id - 10e13)
    result = (8.058077207e-23) * (tmp_id ^ 3) - (2.336540684e-12) * \
        (tmp_id ^ 2) + (3.24366407e-2) * tmp_id + 1242862236
    return int(result)

if __name__ == '__main__':
    # parsing args
    parser = argparse.ArgumentParser(description='Fb registration date estimator')
    parser.add_argument(
        '--csv-file',
        nargs=1,
        required=True,
        help='path to csv file'
    )
    args = parser.parse_args()
    filepath = os.path.abspath(args.csv_file[0])
    if not os.path.exists(filepath):
        print('Please check the file exists: ', filepath)
        exit(-1)
    try:
        file_reader(filepath)
    except Exception as e:
        print(e)
        exit(-2)
